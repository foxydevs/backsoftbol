<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArbitrajeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arbitraje', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('partido')->unsigned()->nullable()->default(null);
            $table->foreign('partido')->references('id')->on('partidos')->onDelete('cascade');
            $table->Integer('torneo')->unsigned()->nullable()->default(null);
            $table->foreign('torneo')->references('id')->on('torneos')->onDelete('cascade');
            $table->Integer('arbitro')->unsigned()->nullable()->default(null);
            $table->foreign('arbitro')->references('id')->on('arbitros')->onDelete('cascade');
            $table->tinyInteger('estado')->nullable()->default(1);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arbitraje');
    }
}
