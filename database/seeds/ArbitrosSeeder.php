<?php

use Illuminate\Database\Seeder;

class ArbitrosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('arbitros')->insert([
            'nombre'           => "Arbitro",
            'apellido'           => "Apellido",
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('arbitraje')->insert([
            'partido'          => 1,
            'torneo'           => 1,
            'arbitro'          => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
    }
}
