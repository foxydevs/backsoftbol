<?php

use Illuminate\Database\Seeder;

class AccesosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('accesos')->insert([
            'agregar'          => 1,
            'modificar'        => 1,
            'mostrar'          => 1,
            'eliminar'         => 1,
            'usuario'          => 1,
            'modulo'           => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
    }
}
