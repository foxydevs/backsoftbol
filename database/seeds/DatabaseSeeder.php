<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(EquiposSeeder::class);
        $this->call(JugadoresSeeder::class);
        $this->call(TorneosSeeder::class);
        $this->call(PartidosSeeder::class);
        $this->call(ArbitrosSeeder::class);
        $this->call(PuntosSeeder::class);
        $this->call(ModulosSeeder::class);
        $this->call(UsuariosSeeder::class);
        $this->call(AccesosSeeder::class);
    }
}
