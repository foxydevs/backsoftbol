<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TiposUsuarios extends Model
{
    use SoftDeletes;
    protected $table = 'tipo_usuarios';
}
