<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Partidos extends Model
{
    use SoftDeletes;
    protected $table = 'partidos';

    public function equipos(){
        return $this->hasMany('App\EquiposPartido','partido','id')->with('equipos');
    }

    public function torneos(){
        return $this->hasOne('App\Torneos','id','torneo');
    }

    public function tipos(){
        return $this->hasOne('App\TiposTorneos','id','tipo');
    }
}
