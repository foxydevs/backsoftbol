<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Penalizaciones extends Model
{
    use SoftDeletes;
    protected $table = 'penalizaciones';
}
