<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EquiposPartido extends Model
{
    use SoftDeletes;
    protected $table = 'equipos_partido';

    public function equipos(){
        return $this->hasOne('App\Equipos','id','equipo');
    }
}
