<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Jugadores extends Model
{
    use SoftDeletes;
    protected $table = 'jugadores';

    public function tipos(){
        return $this->hasOne('App\TiposJugadores','id','tipo');
    }
}
