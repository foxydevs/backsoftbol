<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JugadorEquipo extends Model
{
    use SoftDeletes;
    protected $table = 'jugador_equipo';

    public function jugadores(){
        return $this->hasOne('App\Jugadores','id','jugador');
    }
}
