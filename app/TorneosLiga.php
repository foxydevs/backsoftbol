<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TorneosLiga extends Model
{
    use SoftDeletes;
    protected $table = 'torneos_liga';
}
