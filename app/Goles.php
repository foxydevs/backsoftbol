<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Goles extends Model
{
    use SoftDeletes;
    protected $table = 'goles';
}
