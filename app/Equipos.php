<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Equipos extends Model
{
    use SoftDeletes;
    protected $table = 'equipos';

    public function tipos(){
        return $this->hasOne('App\TiposEquipos','id','tipo');
    }
}
