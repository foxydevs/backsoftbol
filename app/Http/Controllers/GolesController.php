<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Goles;
use App\EquiposPartido;
use Response;
use Validator;

class GolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Goles::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cantidad'          => 'required',
            'jugador'           => 'required',
            'partido'           => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $objectUpdate = Goles::whereRaw('jugador=? and partido=? and equipo=?',[$request->get('jugador'),$request->get('partido'),$request->get('equipo')])->first();
                if ($objectUpdate) {
                    try {
                        $objectUpdate1 = EquiposPartido::whereRaw('partido=? and equipo=?',[$request->get('partido'),$request->get('equipo')])->first();
                        if ($objectUpdate1) {
                            try {
                                $objectUpdate1->resultado = $request->get('cantidad')+($objectUpdate1->resultado-$objectUpdate->cantidad);
                                $objectUpdate1->save();
                            } catch (Exception $e) {
                                $returnData = array (
                                    'status' => 500,
                                    'message' => $e->getMessage()
                                );
                                return Response::json($returnData, 500);
                            }
                        }
                        else {
                            $returnData = array (
                                'status' => 404,
                                'message' => 'No record found'
                            );
                            return Response::json($returnData, 404);
                        }
                        $objectUpdate->cantidad = $request->get('cantidad', $objectUpdate->cantidad);
                        $objectUpdate->save();
                        return Response::json($objectUpdate, 200);
                    } catch (Exception $e) {
                        $returnData = array (
                            'status' => 500,
                            'message' => $e->getMessage()
                        );
                        return Response::json($returnData, 500);
                    }
                }
                else {
                    $newObject = new Goles();
                    $newObject->cantidad          = $request->get('cantidad');
                    $newObject->jugador           = $request->get('jugador');
                    $newObject->partido           = $request->get('partido');
                    $newObject->equipo            = $request->get('equipo');
                    $newObject->save();
                    $objectUpdate = EquiposPartido::whereRaw('partido=? and equipo=?',[$request->get('partido'),$request->get('equipo')])->first();
                    if ($objectUpdate) {
                        try {
                            $objectUpdate->resultado = $request->get('cantidad')+$objectUpdate->resultado;
                    
                            $objectUpdate->save();
                        } catch (Exception $e) {
                            $returnData = array (
                                'status' => 500,
                                'message' => $e->getMessage()
                            );
                            return Response::json($returnData, 500);
                        }
                    }
                    else {
                        $returnData = array (
                            'status' => 404,
                            'message' => 'No record found'
                        );
                        return Response::json($returnData, 404);
                    }
                    return Response::json($newObject, 200);
                }

                                
            } catch (\Illuminate\Database\QueryException $e) {
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Goles::find($id);
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Goles::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->cantidad          = $request->get('cantidad', $objectUpdate->cantidad);
                $objectUpdate->jugador           = $request->get('jugador', $objectUpdate->jugador);
                $objectUpdate->partido           = $request->get('partido', $objectUpdate->partido);
                $objectUpdate->equipo            = $request->get('equipo', $objectUpdate->equipo);
                $objectUpdate->estado            = $request->get('estado', $objectUpdate->estado);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Goles::find($id);
        if ($objectDelete) {
            try {
                Goles::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
