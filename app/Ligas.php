<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ligas extends Model
{
    use SoftDeletes;
    protected $table = 'ligas';
}
