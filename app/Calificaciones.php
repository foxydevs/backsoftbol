<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Calificaciones extends Model
{
    use SoftDeletes;
    protected $table = 'calificaciones';
}
