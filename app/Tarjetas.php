<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tarjetas extends Model
{
    use SoftDeletes;
    protected $table = 'tarjetas';
}
